export default {
  'all-rights-reserved': 'Tous droits réservés',
  currencies: 'Devise | Devises',
  amounts: 'Montant | Montants',
  'required-field': 'Ce champs est obligatoire',
  'converted-amount': 'Montant converti',
  convert: 'Convertir',
  reset: 'Réinitialiser',
  loading: 'Chargement ...',
  'place-request': 'Placez votre demande',
  'convert-amount-to-send': 'Convertir le montant à envoyer',
  'your-name': 'Votre nom',
  'your-email': 'Votre adresse e-mail',
  'recipient-full-name': 'Nom complet du destinataire',
  'recipient-om-momo-number': 'Numéro OM/MoMo du destinataire',
  'min-3-chars': '3 caractères minimum',
  'invalid-email': 'Adresse e-mail invalide',
  'invalid-number': 'Numéro invalide',
  'invalid-orange-mtn-number': 'Numéro Orange/MTN invalide',
  'incorrect-phone-length': 'Longueur du numéro de téléphone invalide',
  'phone-example': 'Ex: 670000000',
  submit: 'Envoyer',
  back: 'Retour',
  'request-placed': 'Demande placée avec succès. Vous recevrez un mail de confirmation dans quelques instants.',
  'proof-of-payment': 'Preuve de paiement',
  'confirm-payment': 'Confirmer le paiement',
  'source-currency': 'Devise d\'origine',
  'target-currency': 'Devise cible',
}
